#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>
#include <json-c/json.h>

void downloadUnzip() { 
  char *url[] = {
    "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp",
    "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT"
  };
  char *filename[]={
      "Characters.zip", "Weapons.zip"
  }

  pid_t child_id;
  int status;

  if ((child_id = fork()) == 0)
    {
  execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
    }
    
  for(int i = 0; i < 2; i++) {
    if((child_id = fork()) == 0) {
      execlp("wget", "wget", "--no-check-certificate", url[i], "-O", filename[i], "-q", NULL);
    }

    while(wait(&status) > 0);

    if((child_id = fork()) == 0) {
      execlp("unzip", "unzip", "-qq", NULL);
    }

    while(wait(&status) > 0);
  }
}

int main() {
  pid_t pid, sid;

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/abi/soal-shift-sisop-modul-2-a04-2022/soal1")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);


  while (1) {
    pid_t child_id;
    int status;

    time_t now = time(NULL);
    struct tm * currTime = localtime(&now);

    if (
      currTime->tm_mday == 30 &&
      currTime->tm_mon  == 3  &&
      currTime->tm_hour == 4  &&
      currTime->tm_min  == 44 &&
      currTime->tm_sec  == 0
    ) 
      while(wait(&status) > 0);

      downloadThenUnzip();
    } else if (
      currTime->tm_mday == 30 &&
      currTime->tm_mon  == 3  &&
      currTime->tm_hour == 7  &&
      currTime->tm_min  == 44 &&
      currTime->tm_sec  == 0
    ) {
      if((child_id = fork()) == 0) {
        execlp("zip", "zip", "-P", "satuduatiga", "not_safe_for_wibu", "-q", "gacha_gacha", NULL);
      }

      while(wait(&status) > 0);
      
      
      if((child_id = fork()) == 0){
        execlp("rm", "rm", "-r", "gacha_gacha", NULL);
            }

    }
